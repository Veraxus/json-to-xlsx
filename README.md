# JSON to XLSX

A simple tool that can flatten any complex JSON object into a "wide" XLSX spreadsheet.

Was created particularly to help in the conversion of Unreal Engine DataTable exports to Excel and back again.

## Install
1. Clone or download this repo
2. Run `npm install`
3. Run `npm link`

## Usage
To convert a JSON file to XLSX:  
`uedt flatten INPUT_FILE.json OUTPUT_FILE.xlsx`

To convert a flattened Excel file back to JSON:  
`uedt expand INPUT_FILE.xlsx OUTPUT_FILE.json`