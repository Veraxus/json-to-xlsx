#!/usr/bin/env node
import { Command } from 'commander'
import flatten from './src/flatten.mjs'
import expand from './src/expand.mjs'

const cli = new Command()

cli
  .name('uedt')
  .description('Flatten UE DataTable JSON exports to CSV, or unflatten CSV back to JSON.')

// Trigger flattening
cli.command('f <json> [csv]')
  .alias('flatten')
  .description('Process a JSON file into a flattened CSV file.')
  .action((json, csv) => {
    flatten(json, csv)
    process.exit(0)
  })

// Trigger expand/unflatten
cli.command('e <csv> [json]')
  .alias('expand')
  .description('Process a flattened CSV file back into a JSON file.')
  .action((csv, json) => {
    expand(csv, json)
    process.exit(0)
  })

cli.command('help', { isDefault: true })
  .action(() => {
    cli.help()
  })

// Begin to parse input
cli.parse(process.argv)