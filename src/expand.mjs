import fs from 'fs'
import XLSX from 'xlsx'
import { changeFileExtension } from './flatten.mjs'

/**
 * Reconstruct a top-level entry object from an XLSX row
 * @param path
 * @param value
 * @returns {{}}
 */
function reconstructEntry (path, value) {
  if (value === '' || (Array.isArray(value) && value.length === 0)) {
    return {}
  }

  const pathParts = path.split('.')
  const result = {}

  let current = result
  for (let i = 0; i < pathParts.length; i++) {
    const part = pathParts[i]
    const isLastPart = i === pathParts.length - 1
    const isArrayIndex = /^\d+$/.test(part)
    const nextPartIsArrayIndex = /^\d+$/.test(pathParts[i + 1])

    if (isLastPart) {
      // END OF PATH: Time to assign a value
      if (isArrayIndex) {
        if (!Array.isArray(current)) {
          current = []
        }
        current[part] = value
      } else {
        current[part] = value
      }
    } else {
      // Continue traversal since we're not at the end of the path yet
      if (isArrayIndex) {
        if (!Array.isArray(current)) {
          current = []
        }
        if (!current[part]) {
          current[part] = nextPartIsArrayIndex ? [] : {}
        }
        current = current[part]
      } else {
        if (!current[part]) {
          current[part] = nextPartIsArrayIndex ? [] : {}
        }
        current = current[part]
      }
    }
  }

  return result
}

/**
 * Recursively rebuild sub-objects for an entry
 * @param target
 * @param source
 * @returns {*}
 */
function mergeDeep (target, source) {
  const isObject = (obj) => obj && typeof obj === 'object'

  if (!isObject(target) || !isObject(source)) {
    return source
  }

  Object.keys(source).forEach((key) => {
    const targetValue = target[key]
    const sourceValue = source[key]

    if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
      sourceValue.forEach((item, index) => {
        if (targetValue[index]) {
          targetValue[index] = mergeDeep(targetValue[index], item)
        } else {
          targetValue[index] = item
        }
      })
    } else if (isObject(targetValue) && isObject(sourceValue)) {
      target[key] = mergeDeep(Object.assign({}, targetValue), sourceValue)
    } else {
      target[key] = sourceValue
    }
  })

  return target
}

/**
 * Read the source XSLX, rebuild JSON, and save reconstructed JSON file
 * @param sourceFile
 * @param targetFile
 */
function expand (sourceFile, targetFile) {

  if (typeof targetFile === 'undefined') {
    targetFile = changeFileExtension(sourceFile, 'json', '_Reconstructed')
  }

  console.log(`Expanding ${sourceFile} to ${targetFile}`)

  const workbook = XLSX.readFile(sourceFile)
  const sheet = workbook.Sheets[workbook.SheetNames[0]]
  const data = XLSX.utils.sheet_to_json(sheet, { header: 1 })

  const headers = data[0]
  const rows = data.slice(1)
  const restoredJSON = []

  rows.forEach((row) => {
    let rowObject = {}

    row.forEach((value, index) => {
      const header = headers[index]
      const partialObject = reconstructEntry(header, value)
      rowObject = mergeDeep(rowObject, partialObject)
    })

    restoredJSON.push(rowObject)
  })

  const jsonOutput = JSON.stringify(restoredJSON, null, 4)

  fs.writeFileSync(targetFile, jsonOutput)
}

export default expand