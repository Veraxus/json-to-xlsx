import fs from 'fs'
import XLSX from 'xlsx'
import chardet from 'chardet'

/**
 * Takes
 * @param obj
 * @param prefix
 * @returns {{}}
 */
function flattenObject (obj, prefix = '', result = {}) {
  for (const key in obj) {
    const newKey = prefix ? `${prefix}.${key}` : key

    if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
      flattenObject(obj[key], newKey, result)
    } else if (Array.isArray(obj[key])) {
      obj[key].forEach((value, index) => {
        if (typeof value === 'object') {
          flattenObject(value, `${newKey}.${index}`, result)
        } else {
          result[`${newKey}.${index}`] = value
        }
      })
    } else {
      result[newKey] = obj[key]
    }
  }

  return result
}

/**
 * Change JSON file extension if no output filename is provided.
 * @param filename
 * @param newExtension
 * @param fileNameSuffix
 * @returns {*|string}
 */
function changeFileExtension (filename, newExtension, fileNameSuffix = '') {
  const lastDotIndex = filename.lastIndexOf('.')
  if (lastDotIndex === -1) {
    return filename
  }
  const filenameWithoutExtension = filename.slice(0, lastDotIndex)
  return `${filenameWithoutExtension}${fileNameSuffix}.${newExtension}`
}

/**
 * Processes a flattened JSON into a CSV string
 *
 * @todo Make sure entries are wrapped in quotes to avoid parsing problems
 * @param flattenedArray
 * @returns {string}
 */
function generateCSV (flattenedArray) {

  // Sets don't allow duplicates...
  const headers = new Set()

  // Pass 1 - Build a set of all property keys
  flattenedArray.forEach((flattenedObj) => {
    Object.keys(flattenedObj).forEach((key) => headers.add(key))
  })
  // Convert set to an array
  const headerArray = Array.from(headers)
  // Convert the array to a CSV row
  let csvOutput = headerArray.join(',') + '\n'

  // Pass 2 - build data rows and place in the correct index
  flattenedArray.forEach((flattenedObj) => {
    // Start by filling the row with blank data equal to the header row length
    const row = new Array(headerArray.length).fill('')

    // Iterate over each entry in the row
    Object.entries(flattenedObj).forEach(([key, value]) => {
      // Find the index/position of this entry based on matching header index
      const columnIndex = headerArray.indexOf(key)

      if (columnIndex !== -1) {
        // Found proper location, insert there
        row[columnIndex] = value
      } else {
        console.error(`Key ${key} was not found in header array!`)
        process.exit(1)
      }
    })

    // Convert row array to CSV string and append
    csvOutput += row.join(',') + '\n'
  })

  return csvOutput
}

/**
 * Generate an XLSX file string
 * @param flattenedArray
 * @returns {any[][]}
 */
function generateXLSX(flattenedArray) {
  const headers = new Set();

  flattenedArray.forEach((flattenedObj) => {
    Object.keys(flattenedObj).forEach((key) => headers.add(key));
  });

  const headerArray = Array.from(headers);
  const data = [headerArray];

  flattenedArray.forEach((flattenedObj) => {
    const row = new Array(headerArray.length).fill('');
    Object.entries(flattenedObj).forEach(([key, value]) => {
      const columnIndex = headerArray.indexOf(key);
      if (columnIndex !== -1) {
        row[columnIndex] = value;
      }
    });
    data.push(row);
  });

  return data;
}

/**
 * Ingest json, trigger processing, and save csv.
 * @param sourceFile
 * @param targetFile
 */
function flatten (sourceFile, targetFile, encoding) {
  if (!fs.existsSync(sourceFile)) {
    console.error(`File ${sourceFile} does not exist.`)
    process.exit(1)
  }

  if (typeof targetFile === 'undefined') {
    targetFile = changeFileExtension(sourceFile, 'xlsx', '_Flattened')
  }

  if (typeof encoding === 'undefined') {
    encoding = chardet.detectFileSync(sourceFile)
    if ( encoding !== 'UTF-16LE') {
      encoding = 'utf8'
    }
  }

  console.log(`Flattening ${sourceFile} to ${targetFile}`)

  // Read in the JSON
  const contents = fs.readFileSync(sourceFile, { encoding })
  // Strip the BOM at the start of the file
  const contentsNoBom = contents.replace(/^.*\[/, '[')
  const json = JSON.parse(contentsNoBom)

  // Parse and process JSON to flat CSV
  const flattenedArray = json.map(obj => flattenObject(obj))
  
  // Generate XLSX file from data
  const excelData = generateXLSX(flattenedArray);
  const wb = XLSX.utils.book_new();
  const ws = XLSX.utils.aoa_to_sheet(excelData);
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, targetFile);

  // Generate the CSV file from the data
  //const csvString = generateCSV(flattenedArray)
  // write CSV file
  //fs.writeFileSync(target, csvString)
}

export default flatten

export { changeFileExtension }